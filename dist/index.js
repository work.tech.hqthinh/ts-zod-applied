"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const zod_1 = require("zod");
// creating a schema for strings
const UserSchema = zod_1.z.object({
    id: zod_1.z.string().email(),
    name: zod_1.z.string(),
});
const maybeUser = {
    id: "123",
    name: "HQTH",
};
const user = UserSchema.parse(maybeUser);
//--------------LOGIN FORM SCHEMA ------------
const LoginSchema = zod_1.z.object({
    email: zod_1.z.string().email(),
    password: zod_1.z.string(),
});
const useForm = (schema, onSubmit) => {
    return {
        onSubmit: (values) => {
            schema.parse(values);
            onSubmit(values);
        },
    };
};
const form = useForm(LoginSchema, (values) => {
    console.log(values);
});
form.onSubmit(() => { });
//# sourceMappingURL=index.js.map