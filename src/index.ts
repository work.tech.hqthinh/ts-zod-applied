import { number, Schema, z } from "zod";

// creating a schema for strings
const UserSchema = z.object({
    id: z.string().email(),
    name: z.string(),
});

type User = z.infer<typeof UserSchema>;

const maybeUser: User = {
    id: "123",
    name: "HQTH",
};


const user = UserSchema.parse(maybeUser);

//--------------LOGIN FORM SCHEMA ------------
const LoginSchema = z.object({
    email: z.string().email(),
    password: z.string(),
    age: z.number().optional(),
});


const loginTest = {
    email: "hqth@netcompany.com",
    password: "2321",
}


const useForm = <TValues>(schema: z.Schema<TValues>, onSubmit: (values: TValues) => void) => {
    return {
        onSubmit: (values: TValues) => {
            schema.parse(values);
            onSubmit(values);
        },
    }
}

const form = useForm(LoginSchema, (values) => {
    console.log(values);
});

form.onSubmit(loginTest);